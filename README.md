# 03 Interoperabilita priestorových údajov

Priestor na diskusiu a dokumentáciu príkladov pre problematiku interoperability a harmonizácie priestorových údajov.
Jednotlivé príspevky je možné vytvárať [v časti Issues](https://gitlab.com/mzpsr/mzpsr/podpora-inspire-implement-cie/03-interoperabilita-priestorov-ch-dajov/issues).
Dokumentácia príkladov dobrej praxe a výsledkov diskusií je k dispozícii [na Wiki Dobrej praxe](https://gitlab.com/mzpsr/mzpsr/podpora-inspire-implement-cie/03-interoperabilita-priestorov-ch-dajov/wikis/Dobr%C3%A1-prax-pre-harmoniz%C3%A1ciu-priestorov%C3%BDch-%C3%BAdajov).